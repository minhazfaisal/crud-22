<?php

// connection to db
$db = new PDO('mysql:host=localhost;dbname=bitmphp52;charset=utf8mb4', 'root', '');

//build query
$query = "SELECT * FROM `students` WHERE id = ".$_GET['id'];

//execute the query using php
foreach ($db->query($query) as $row){
    $student = $row;
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>


<div class="container">
    <div class="row">
        <div class="col-md-4">
            <nav>
                <li><a href="index.php">All Students</a></li>
            </nav>
            <form action="update.php" method="post">
                <fieldset>
                    <legend>Student Infomation</legend>

                    <input type="hidden" class="form-control" id="id" name="id" placeholder="Enter First Name"
                           value="<?=$student['id'];?>">

                    <div class="form-group">
                        <label for="first_name">First Name :</label>
                        <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Enter First Name"
                        value="<?=$student['first_name'];?>">
                    </div>
                    <div class="form-group">
                        <label for="last_name">Last Name :</label>
                        <input value="<?=$student['last_name'];?>" type="text" class="form-control" id="last_name" name="last_name" placeholder="Enter Last Name">
                    </div>
                    <div class="form-group">
                        <label for="seip">SEIP ID</label>
                        <input value="<?=$student['seip'];?>" type="text" class="form-control" id="seip" name="seip" placeholder="Enter Your SEIP ID">
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                </fieldset>
            </form>
        </div>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../assets/js/bootstrap.min.js"></script>
</body>
</html>